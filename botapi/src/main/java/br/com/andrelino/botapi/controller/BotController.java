package br.com.andrelino.botapi.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.andrelino.botapi.dto.BotDTO;
import br.com.andrelino.botapi.model.Bot;
import br.com.andrelino.botapi.service.BotService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/bots")
@Api(tags = {"Performs CRUD operations for bots."})
public class BotController {
	
	@Autowired
	private BotService botService;
	
	@GetMapping	
	@ApiOperation(value = "Search all bots.")
	public List<Bot> get(){
		return botService.get();
	}
	
	@GetMapping("/{id}")
	@ApiOperation(value = "Search bot by id.", notes = "Id in UUID format")
	public Bot get(@PathVariable("id") UUID id) {
		return botService.get(id);
	}
	
	@PostMapping
	@ApiOperation(value = "Create a new bot.")
	public Bot post(@RequestBody BotDTO botDTO) {
		return botService.post(botDTO);
	}
	
	@PutMapping("/{id}")
	@ApiOperation(value = "Change a bot.", notes = "Id in UUID format")
	public Bot put(@PathVariable("id") UUID id, @RequestBody BotDTO botDTO) {
		Bot botOrigem = validateBot(id);
		try {
			return botService.put(botOrigem, botDTO);	
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failure to change", e);
		}		
	}

	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Delete a bot.", notes = "id in UUID format")
	public ResponseEntity<String> delete(@PathVariable("id") UUID id) {
		Bot botOrigem = validateBot(id);
		try {
			return new ResponseEntity<>(botService.delete(botOrigem), HttpStatus.OK);	
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Exclusion failure", e);
		}		
	}
	
	private Bot validateBot(UUID id) {
		Bot botOrigem = botService.get(id);
		if (botOrigem.getId() == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Bot not found");			
		}
		return botOrigem;
	}
}
