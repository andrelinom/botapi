package br.com.andrelino.botapi.dto;

import java.time.LocalDateTime;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class MessageDTO {
	private UUID conversationid;
	@JsonProperty("timestamp")
	private LocalDateTime datenow;
	@JsonProperty("from")
	private UUID sender;
	@JsonProperty("to")
	private UUID recipient;
	@JsonProperty("text")
	private String message;
}
