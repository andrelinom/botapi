package br.com.andrelino.botapi.repository;

import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;

import br.com.andrelino.botapi.model.Bot;

public interface BotRepository extends CassandraRepository<Bot, UUID>{

}
