package br.com.andrelino.botapi.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.andrelino.botapi.dto.MessageDTO;
import br.com.andrelino.botapi.model.Bot;
import br.com.andrelino.botapi.model.Message;
import br.com.andrelino.botapi.service.BotService;
import br.com.andrelino.botapi.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/messages")
@Api(tags = {"Messages exchanges between User and Bot."})
public class MessageController {
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private BotService botService;
	
	private final String NOTE_POST = "To start a conversation, you must send a message in the field <FROM> a registered bot, the timestamp (optional, if you do not send the local API), and the <text> field with the message.\n" + 
			"Ex. {\n" + 
			"    \"from\": \"379f6cea-fcf8-45b6-9ebc-32c8d9ad67a2\",\n" + 
			"   \"text\": \"Hi, how can I help you?\",\n" + 
			"   \"timestamp\": \"2019-07-14T15: 55: 44.695Z\",\n" + 
			"}\n\n"+
			"To continue the conversation it is necessary to inform all the fields, according to the response.\n" + 
			"Ex.{\n" + 
			"      \"conversationid\": \"dc377027-af59-4c26-a8f5-dadfcd94f637\",\n" + 
			"     \"timestamp\": \"2019-07-14T15: 46: 43.007\",\n" + 
			"     \"to\": \"71330b78-c239-4d92-bc25-6abbeb0fdfc0\",\n" + 
			"     \"from\": \"86f07eb3-16e4-4b46-b9af-f1c934335c40\",\n" + 
			"     \"text\": \"Would you like to know my balance ?!\"\n" + 
			"   }";

	
	@GetMapping("/{id}")
	@ApiOperation(value = "Search message by id.", notes = "Id in UUID format")
	public Message get(@PathVariable("id") UUID id) {		
		return validateMessage(id);
	}
	
	@GetMapping("/conversationId={conversationId}")
	@ApiOperation(value = "Search message by id.", notes = "conversationId in UUID format")
	public List<Message> getConversationId(@PathVariable("conversationId") UUID conversationId) {		
		return validateMessageConversationId(conversationId);
	}
	
	@PostMapping
	@ApiOperation(value = "Send message.", notes = NOTE_POST)
	public Message post(@RequestBody MessageDTO message) {
		validateMessage(message);		
		try {
			return messageService.post(message);	
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Sending the message failed", e);
		}
	}

	private void validateMessage(MessageDTO message) {
		if (message.getSender() == null) {
			throw new ResponseStatusException(HttpStatus.FORBIDDEN, "The from is required");
		}
		if (message.getRecipient() != null) {
			Bot botOrigem = botService.get(message.getRecipient());
			if (botOrigem.getId() == null) {
				botOrigem = botService.get(message.getSender());
				if (botOrigem.getId() == null) {
					throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There is no Bot in the Message");
				}
			}
		} else {
			Bot botOrigem = botService.get(message.getSender());
			if (botOrigem.getId() == null) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There is no Bot in the Message");
			}			
		}
	}
	
	private Message validateMessage(UUID id) {
		Message message = messageService.get(id);
		if (message.getId() == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Message not found");			
		}
		return message;
	}
	
	private List<Message> validateMessageConversationId(UUID conversationId) {
		List<Message> messages = messageService.getConversationId(conversationId);
		if (messages.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Messages not found");			
		}
		return messages;
	}
}
