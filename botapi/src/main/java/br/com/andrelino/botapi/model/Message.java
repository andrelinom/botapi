package br.com.andrelino.botapi.model;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Table(value = "messages")
@Getter
@Setter
public class Message {
	@PrimaryKey
	private UUID id;
	@Indexed
	private UUID conversationid;
	@JsonProperty("timestamp")
	private LocalDateTime datenow;
	@JsonProperty("from")
	private UUID sender;
	@JsonProperty("to")
	private UUID recipient;
	@JsonProperty("text")
	private String message;
	
}
