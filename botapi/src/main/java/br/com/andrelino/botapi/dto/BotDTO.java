package br.com.andrelino.botapi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BotDTO {
	private String name;
}
