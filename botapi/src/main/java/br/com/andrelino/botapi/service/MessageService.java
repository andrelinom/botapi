package br.com.andrelino.botapi.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datastax.driver.core.utils.UUIDs;

import br.com.andrelino.botapi.dto.MessageDTO;
import br.com.andrelino.botapi.model.Message;
import br.com.andrelino.botapi.repository.MessageRepository;

@Service
public class MessageService {

	@Autowired
	private MessageRepository messageRepository;

	public Message get(UUID id) {
		return messageRepository
				.findById(id)
				.orElse(new Message());
	}

	public Message post(MessageDTO messageDTO) throws Exception {
		Message message = new Message();
		message.setId(UUIDs.random());
		message.setConversationid((messageDTO.getConversationid() != null) ? messageDTO.getConversationid() : UUIDs.random());
		
		message.setSender(messageDTO.getSender());
		message.setRecipient(messageDTO.getRecipient() == null ? UUIDs.random() : messageDTO.getRecipient());
		message.setDatenow(messageDTO.getDatenow() == null ? LocalDateTime.now() : messageDTO.getDatenow());	
		message.setMessage(messageDTO.getMessage());
		
		return messageRepository.save(message);		
	}
	
	public List<Message> getConversationId(UUID conversationid) {
		return messageRepository.findByConversationid(conversationid);
	}
	
	
}
