package br.com.andrelino.botapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableEurekaClient
public class BotapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BotapiApplication.class, args);
	}

}
