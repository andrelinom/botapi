package br.com.andrelino.botapi.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datastax.driver.core.utils.UUIDs;

import br.com.andrelino.botapi.dto.BotDTO;
import br.com.andrelino.botapi.model.Bot;
import br.com.andrelino.botapi.repository.BotRepository;

@Service
public class BotService {
	@Autowired
	private BotRepository botRepository;

	public List<Bot> get() {		
		return botRepository.findAll();
	}

	public Bot get(UUID id) {		
		return botRepository
					.findById(id)
					.orElse(new Bot());
	}

	public Bot post(BotDTO botDTO) {
		Bot bot = new Bot();
		bot.setId(UUIDs.random());
		bot.setName(botDTO.getName());
		return botRepository.save(bot);
	}

	public Bot put(Bot botOrigem, BotDTO botDTO) throws Exception {
		botOrigem.setName(botDTO.getName());
		return botRepository.save(botOrigem);		
	}

	public String delete(Bot botOrigem) throws Exception {
		botRepository.delete(botOrigem);
		return "Successfully deleted";
	}
	
	
}
