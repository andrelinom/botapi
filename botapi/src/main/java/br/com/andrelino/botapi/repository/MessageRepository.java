package br.com.andrelino.botapi.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;

import br.com.andrelino.botapi.model.Message;

public interface MessageRepository extends CassandraRepository<Message, UUID>{	
	List<Message> findByConversationid(UUID conversationid);
}
