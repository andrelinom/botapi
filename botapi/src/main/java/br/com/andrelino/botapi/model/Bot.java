package br.com.andrelino.botapi.model;

import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import lombok.Getter;
import lombok.Setter;

@Table(value = "bots")
@Getter
@Setter
public class Bot {

	@PrimaryKey
	private UUID id;
	
	private String name;
}
